<?php 
$host = 'localhost';

$database = 'crud_multiple';

define("DSN", "mysql:host=$host;dbname=$database");

define("USERNAME", "root");

define("PASSWORD", "root");

//Custom PDO options.
$options = array(PDO::ATTR_PERSISTENT => true);

try {
    
    $dbh = new PDO(DSN, USERNAME, PASSWORD, $options);

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //echo "Connection Sucessfull";

} catch (PDOException $e) {
    //handle error
    echo "Connection error: ".$e->getMessage();
}