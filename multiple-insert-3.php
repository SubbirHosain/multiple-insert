<?php
	include 'db.php';

/*    INSERT INTO `tbl_crud` (`fname`, `lname`, `city`, `lang`) VALUES (NULL, 'Subbir', 'Hosain', 'Dhaka', 'Bangladesh'), (NULL, 'Hosain', 'Subbir', 'Sylet', 'Bangladesh') */


// insert into table (fielda, fieldb, ... ) values (?,?...), (?,?...)
//$sql = "INSERT INTO table (" . implode(",", $datafields ) . ") VALUES " . implode(',', $question_marks);
    
    //do some validation for at least one insertion
    $numbers = $_POST['numbers'];

    $dbh->beginTransaction(); // also helps speed up your inserts.

    $sql = "INSERT INTO tbl_crud(fname,lname,city,lang) values";

    $insert_values = array();
    for ($i=0; $i <$numbers ; $i++) { 
        $sql .= "(?,?,?,?),";
 
        $data = array($_POST['name'][$i],$_POST['surname'][$i],$_POST['city'][$i],$_POST['language'][$i]);

        $insert_values = array_merge($insert_values, array_values($data));

    }
    $sql = rtrim($sql,",");

    $stmt = $dbh->prepare ($sql);
    try {
        $result = $stmt->execute($insert_values);
        if ($result) {
            echo "Records saved";
        }

    } catch (PDOException $e){
        echo $e->getMessage();
    }
    $dbh->commit();
?>
